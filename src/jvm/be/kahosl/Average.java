package be.kahosl;

import storm.trident.operation.CombinerAggregator;
import storm.trident.tuple.TridentTuple;


public class Average implements CombinerAggregator<Double> {

    @Override
    public Double init(TridentTuple tuple) {
        return (Double) tuple.getValue(0);
    }

    @Override
    public Double combine(Double val1, Double val2) {
        return new Double((val1+val2)/2);
    }

    @Override
    public Double zero() {
        return 0D;
    }
    
}