package be.kahosl.spout;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import be.kahosl.E5SensorValue;

public class E5Spout extends BaseRichSpout {

	private SpoutOutputCollector collector;
	private ArrayList<E5SensorValue> sensorValues = new ArrayList<E5SensorValue>();
	private int counter, year;

	public E5Spout(int year) {
		this.year = year;
	}
	
	@Override
	public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
		this.collector = collector;
		// read sensor values from E8 file
		try {
			Scanner s = new Scanner(new File("E5-" + this.year + ".txt")).useDelimiter("\n");
			int lines = 0;
			while (s.hasNext()) {
				String line = s.next().replaceAll("[\\t\\r]", " ");
				if (lines > 1) {
					String [] fields = line.split(" ");
				    this.sensorValues.add(new E5SensorValue(String.valueOf(this.year), fields[0], fields[1], Double.parseDouble(fields[2])));
				}
				lines++;
			}
			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		this.counter = 0;
		
	}
	@Override
	public void nextTuple() {
		//every 0.1s
        Utils.sleep(50);
        E5SensorValue value = this.sensorValues.get(counter);

        this.collector.emit(new Values(value.getSensorId(),  value.getDay().replaceAll(String.valueOf(year), "2011"), value.getTime(), String.valueOf(value.getKwh())));
        counter++;
		
	}
	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("id", "day", "time", "kwh"));	
		
	}
	
	
}
